<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
	$('.brand-carousel2').owlCarousel({
  loop:true,
  margin:10,
  autoplay:true,
  responsive:{
    0:{
      items:1
    },
    600:{
      items:1
    },
    1000:{
      items:1
    }
  }
}) 

</script>
<div class="global__heading">
	
	<?php
		function the_breadcrumb() {
			if (!is_home()) {
				echo '<a href="';
				echo get_option('home');
				echo '">';
				bloginfo('name');
				echo "</a> ";
				if (is_category() || is_single()) {
					// the_category('title_li=');
					if (is_single()) {
						echo " > ";
						the_title();
					}
				} elseif (is_page()) {
					echo the_title();
				}
			}
		}
	?>
	<div class="blog__breadcrumb">
   <?php the_breadcrumb(); ?>
	</div>
	<?php
		get_template_part( 'template-parts/entry-header' );
	?>
</div>
<!-- Single Blogs Heading Part -->
<div class="single__blogs__header">
<?php

$archive_title    = '';
$archive_subtitle = '';

if ( is_search() ) {
	global $wp_query;

	$archive_title = sprintf(
		'%1$s %2$s',
		'<span class="color-accent">' . __( 'Search:', 'twentytwenty' ) . '</span>',
		'&ldquo;' . get_search_query() . '&rdquo;'
	);

	if ( $wp_query->found_posts ) {
		$archive_subtitle = sprintf(
			/* translators: %s: Number of search results. */
			_n(
				'We found %s result for your search.',
				'We found %s results for your search.',
				$wp_query->found_posts,
				'twentytwenty'
			),
			number_format_i18n( $wp_query->found_posts )
		);
	} else {
		$archive_subtitle = __( 'We could not find any results for your search. You can give it another try through the search form below.', 'twentytwenty' );
	}
} elseif ( is_archive() && ! have_posts() ) {
	$archive_title = __( 'Nothing Found', 'twentytwenty' );
} elseif ( ! is_home() ) {
	$archive_title    = get_the_archive_title();
	$archive_subtitle = get_the_archive_description();
}

if ( $archive_title || $archive_subtitle ) {
	?>		
	
</div>
<!-- end Single Blogs Heading Part-->

<main id="site-content" role="main">
<div class="main__content__blogarea">
	<!-- move to top header section-->
<?php
}

if ( have_posts() ) {

	$i = 0;

	while ( have_posts() ) {
		$i++;
		if ( $i > 1 ) {
			echo '<hr class="post-separator styled-separator is-style-wide section-inner" aria-hidden="true" />';
		}
		the_post();

		get_template_part( 'template-parts/content', get_post_type() );

	}
} elseif ( is_search() ) {
	?>
	

		<div class="no-search-results-form section-inner thin">

			<?php
			get_search_form(
				array(
					'label' => __( 'search again', 'twentytwenty' ),
				)
			);
			?>

		</div><!-- .no-search-results -->

		<?php
	}
	?>
<!-- Related post -->
<h4 class="heading4">RELATED <b>POSTS</b></h4>
<div class="brand-carousel owl-carousel">
       <?php
		$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 6, 'post__not_in' => array($post->ID) ) );
		if( $related ) foreach( $related as $post ) {
		setup_postdata($post); ?>
		<div class="single-logo">
		<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>">
		  <?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
				the_post_thumbnail('full');
			} ?>
			</a>
			<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>" class="heading__related"><?php the_title(); ?></a>
			<span class="related__postcontent">
			<?php the_excerpt(); ?>
		    </span>
			<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>" class="read__more2">read more <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
			</div>
		<?php }
		wp_reset_postdata(); ?>
</div>
<!--End Related Post -->
	
</div>
<div class="sidebar__customblog list--sidebar">
  <?php dynamic_sidebar( 'sidebar-6' ); ?>

<!-- Recent Post-->
<div class="brand-carousel2 owl-carousel recent--post--modified" id="siderbar-sticky">
	<h2>RECENT POSTS</h2>
    <?php
        $topNews = new WP_Query();
        $topNews->query('showposts=5'); 
        while ($topNews->have_posts()) : $topNews->the_post(); ?>
            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="recent__post1">
				<div class="recent__post2">
				<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'android_and_tea' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark">
					<?php if ( has_post_thumbnail() ) { 
					the_post_thumbnail('full');
					} ?>
				</a>
				</div>
				<div class="recent__post3">
				<h4 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'android_and_tea' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h4>
				<span><?php echo get_the_date('M d, Y'); ?></span>
			</div>
			</div>    
    <?php endwhile; ?>
	
</div>
<!--End Recent Post -->
<?php dynamic_sidebar( 'sidebar-7' ); ?>

</div>
</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>
