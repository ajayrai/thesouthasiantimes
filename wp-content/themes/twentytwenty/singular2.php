<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<div class="global__heading">
	
	<?php
		function the_breadcrumb() {
			if (!is_home()) {
				echo '<a href="';
				echo get_option('home');
				echo '">';
				bloginfo('name');
				echo "</a> ";
				if (is_category() || is_single()) {
					// the_category('title_li=');
					if (is_single()) {
						echo " > ";
						the_title();
					}
				} elseif (is_page()) {
					echo the_title();
				}
			}
		}
	?>
	<div class="blog__breadcrumb">
   <?php the_breadcrumb(); ?>
	</div>
	<?php
		get_template_part( 'template-parts/entry-header' );
	?>
</div>
<main id="site-content" role="main">
<div class="main__content__blogarea">
	<?php

	if ( have_posts() ) {

		while ( have_posts() ) {
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );
		}
	}

	?>
<!-- Related Post-->
<h4 class="heading4">RELATED <b>POSTS</b></h4>
<div class="brand-carousel owl-carousel">
       <?php
		$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 6, 'post__not_in' => array($post->ID) ) );
		if( $related ) foreach( $related as $post ) {
		setup_postdata($post); ?>
		<div class="single-logo">
		<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>">
		  <?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
				the_post_thumbnail('full');
			} ?>
			</a>
			<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>" class="heading__related"><?php the_title(); ?></a>
			<span class="related__postcontent">
			  <?php the_content('Read the rest of this entry &raquo;'); ?>
		    </span>
			<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>" class="read__more2">read more <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
			</div>
		<?php }
		wp_reset_postdata(); ?>
</div>
<!--End Related Post -->
</div>
</div>
<div class="sidebar__customblog">
<!-- Newsletter Sidebar -->
	<div class="newletter--sidebar">
	<?php dynamic_sidebar( 'sidebar-6' ); ?>
	</div>
<!-- End -->
<!-- Recent Post-->
	<div class="recentpost--sidebar" id="siderbar-sticky">
		<h2>RECENT POSTS</h2>
		<div class="brand-carousel2 owl-carousel recent--post--modified">
			<?php
			$topNews = new WP_Query();
			$topNews->query('showposts=5'); 
			while ($topNews->have_posts()) : $topNews->the_post(); ?>
			    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="recent__post1">
						<div class="recent__post2">
							<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'android_and_tea' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark">
							<?php if ( has_post_thumbnail() ) { 
							the_post_thumbnail('full');
							} ?>
							</a>
						</div>
						<div class="recent__post3">
							<h4 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'android_and_tea' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h4>
							<span><?php echo get_the_date('M d, Y'); ?></span>
						</div>
					</div>
				<!-- </div> -->
			<?php endwhile; ?>
		</div>

	</div>
<!-- End -->
<!-- Add Rotator Post-->
<div class="addtotator--sidebar">
	<?php dynamic_sidebar( 'sidebar-7' ); ?>
	</div>
<!-- End -->
</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>
<script>
window.onscroll = function() {myFunction()};
var header = document.getElementById("siderbar-sticky");
var sticky = header.offsetTop;
function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky__sidebar");
  } else {
    header.classList.remove("sticky__sidebar");
  }
}
</script>
<script>
	$('.brand-carousel2').owlCarousel({
  loop:true,
  margin:10,
  autoplay:true,
  responsive:{
    0:{
      items:1
    },
    600:{
      items:1
    },
    1000:{
      items:1
    }
  }
}) 

</script>