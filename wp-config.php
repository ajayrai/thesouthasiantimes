<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'thesouthasiantimes' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'mk1gkjMc!GqN(:)QHE*?y]nzF{7rK5@U0XT%>+8Yck,<gzQb OuJK3(YO%,~!YF#' );
define( 'SECURE_AUTH_KEY',  '9+r?dr5;]1<&nY}nNsx2uZtV:J^pPoMlWa4,,m>2G>!klUYNQ@`5etgtFPPzy#n4' );
define( 'LOGGED_IN_KEY',    '-1wzo-G D!:1OGdjQ8T+Df3]:2K^D(.hffadz|po<nXn(]`&l2)eYyc7HJ?sa8R?' );
define( 'NONCE_KEY',        '6L@K5VO.y(+o-Z#jo.Xw f@{XCVPm+!wS0wV8hCf 3B]/nK?P+aSAloNd!@/~5NI' );
define( 'AUTH_SALT',        'uv8)) |/iqgH}X-;UTLo!%t}ruVKsLno*WrjtWqkwz6B:PcB/|)*6<$#=M0Y!Yp]' );
define( 'SECURE_AUTH_SALT', '9ehCSm1!x!JduD/RV30u;.OFVKvi:$Mo&6^Wao,IpL3Z6efX}:b~c{<xzE/5bld2' );
define( 'LOGGED_IN_SALT',   '3D73R*,@mS:^M8T0O;JicZ1#OA#4awStj7N}K+jaq=0vveWXo2.q+T*/<w(hfGv=' );
define( 'NONCE_SALT',       'TlsVF0L${)lvTl9)SJ[iUH+>9OgG(:7~R/)c93t}f!U_~LQVZdRvOwDG0avG3qGx' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
